﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VisitListMVC.Models.Visits
{
    public class VisitInventory
    {
        public List<Visit> VisitList { get; } = new();
    }
}
