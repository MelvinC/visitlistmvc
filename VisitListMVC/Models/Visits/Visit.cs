﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VisitListMVC.Models.Visits
{
    public class Visit
    {
        public int Id { get; set; }

        [Required]
        public string? Name { get; set; }

        public Localisation localisation { get; set; }

        public float longitude
        {
            get
            {
                return localisation.longitude;
            }
        }
        public float latitude
        {
            get
            {
                return localisation.latitude;
            }
        }

        public Date? StartDate { get; set; }
        public Date? EndDate { get; set; }

        public string Comment { get; set; }

        public List<string> PhotoList { get; set; } = new();

        public string getStartDateDisplay()
        {
            return StartDate != null ? StartDate.toInputDisplay() : "";
        }

        public string getEndDateDisplay()
        {
            return EndDate != null ? EndDate.toInputDisplay() : "";
        }
    }
}