﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VisitListMVC.Models.Visits
{
    public static class VisitService
    {
        public static VisitInventory VisitList { get; } = new();

        static string jsonFile = "visits.json";
        static int nextId = 3;
        static VisitService()
        {
            var directory = Directory.GetCurrentDirectory();
            var foundFiles = Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "visits.json");
            if (foundFiles != null && foundFiles.Count() > 0)
            {
                jsonFile = foundFiles.ElementAt(0);
            }

            addVisit(jsonFile);
        }

        public static void addVisit(string file)
        {
            string visitJson = File.ReadAllText(file);
            List<Visit> visit = JsonConvert.DeserializeObject<List<Visit>>(visitJson);

            if (visit != null)
            {
                VisitList.VisitList.AddRange(visit);
            }
        }

        public static List<Visit> GetAll() => VisitList.VisitList;

        public static VisitInventory GetInventory() => VisitList;

        public static Visit Get(int id) => VisitList.VisitList.FirstOrDefault(v => v.Id == id);

        public static Visit Get(string name) => VisitList.VisitList.FirstOrDefault(v => v.Name == name);

        public static void Add(Visit visit)
        {
            visit.Id = nextId++;
            VisitList.VisitList.Add(visit);
        }

        public static void Delete(int id)
        {
            var visit = Get(id);
            if (visit is null)
                return;

            VisitList.VisitList.Remove(visit);
        }

        public static int GetJsonIdFromVisitId(int id)
        {
            var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
            int jsonIndex = 0;
            while (jsonIndex < visitListObj.Count())
            {
                var token = visitListObj[jsonIndex]["Id"];
                if (token != null && token.Value<int>() == id)
                {
                    break;
                }
                ++jsonIndex;
            }
            return jsonIndex;
        }
        public static string StartDateText(Visit visit)
        {
            if (visit.StartDate == null)
            {
                return "";
            }
            return visit.StartDate.ToString();
        }

        public static string EndDateText(Visit visit)
        {
            if (visit.EndDate == null)
            {
                return "";
            }
            return visit.EndDate.ToString();
        }

        public static void Update(Visit visit)
        {
            var index = VisitList.VisitList.FindIndex(v => v.Id == visit.Id);
            if (index == -1)
                return;

            var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
            int jsonIndex = GetJsonIdFromVisitId(visit.Id);

            if (jsonIndex < visitListObj.Count())
            {
                visitListObj[jsonIndex]["Name"] = visit.Name;

                var startDate = visitListObj[jsonIndex]["StartDate"];
                if (startDate != null && visit.StartDate != null)
                {
                    startDate["day"] = visit.StartDate.day;
                    startDate["month"] = visit.StartDate.month;
                    startDate["year"] = visit.StartDate.year;
                }
                else
                {
                    visit.StartDate = VisitList.VisitList[index].StartDate;
                }

                var endDate = visitListObj[jsonIndex]["EndDate"];
                if (endDate != null && visit.EndDate != null)
                {
                    endDate["day"] = visit.EndDate.day;
                    endDate["month"] = visit.EndDate.month;
                    endDate["year"] = visit.EndDate.year;
                }
                else
                {
                    visit.EndDate = VisitList.VisitList[index].EndDate;
                }

                visitListObj[jsonIndex]["Comment"] = visit.Comment;

                visit.PhotoList = VisitList.VisitList[index].PhotoList;

                string output = Newtonsoft.Json.JsonConvert.SerializeObject(visitListObj, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText("visits.json", output);

                VisitList.VisitList[index] = visit;
            }
        }

        public static void DeletePhoto(int visitId, string photo)
        {
            var index = VisitList.VisitList.FindIndex(v => v.Id == visitId);
            if (index == -1)
                return;

            Visit visit = VisitList.VisitList[index];
            visit.PhotoList.Remove(photo);

            var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
            int jsonIndex = GetJsonIdFromVisitId(visitId);
            var jsonPhotoList = visitListObj[jsonIndex]["PhotoList"];
            if (jsonPhotoList != null && jsonPhotoList.Count() > 0)
            {
                List<JToken> photoToRemoveList = new();
                foreach (var jsonPhoto in jsonPhotoList)
                {
                    var jsonPhotoLink = jsonPhoto.Value<string>();
                    if (jsonPhotoLink != null && jsonPhotoLink.Equals(photo))
                    {
                        photoToRemoveList.Add(jsonPhoto);
                    }
                }
                foreach (var photoToRemove in photoToRemoveList)
                {
                    photoToRemove.Remove();
                }
            }

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(visitListObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("visits.json", output);

            string photoFolder = "wwwRoot\\Photos";
            if (File.Exists(Path.Combine(photoFolder, photo)))
            {
                File.Delete(Path.Combine(photoFolder, photo));
            }
        }

        public static void AddPhoto(int visitId, string photoName)
        {
            var index = VisitList.VisitList.FindIndex(v => v.Id == visitId);
            if (index == -1)
                return;

            Visit visit = VisitList.VisitList[index];
            visit.PhotoList.Add(photoName);

            var visitListObj = JArray.Parse(File.ReadAllText(jsonFile));
            int jsonIndex = GetJsonIdFromVisitId(visit.Id);
            JArray jsonPhotoList = new();
            var oldPhotoList = visitListObj[jsonIndex]["PhotoList"];
            if (oldPhotoList != null)
            {
                foreach (var photo in oldPhotoList)
                {
                    jsonPhotoList.Add(photo);
                }
            }
            jsonPhotoList.Add(photoName);
            visitListObj[jsonIndex]["PhotoList"] = jsonPhotoList;

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(visitListObj, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("visits.json", output);
        }
    }
}