﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VisitListMVC.Models.Visits;

namespace VisitListMVC.Controllers
{
    public class MapController : Controller
    {
        public VisitInventory visitList = new();

        public IActionResult Map()
        {
            visitList = VisitService.GetInventory();
            return View(visitList);
        }
    }
}
