﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VisitListMVC.Models.Visits;

namespace VisitListMVC.Controllers
{
    public class VisitsController : Controller
    {
        public List<Visit> visitList = new();

        public IActionResult Visits()
        {
            visitList = VisitService.GetAll();
            return View();
        }

        public IActionResult Delete(int id)
        {
            VisitService.Delete(id);
            return RedirectToAction("Visits");
        }
        public IActionResult Update(Visit visit)
        {
            return RedirectToPage("/ModifyVisit", new { visitId = visit.Id });
        }
    }
}
