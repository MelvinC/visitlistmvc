using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace VisitListMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NTc4MjY3QDMxMzkyZTM0MmUzMENSd1NqRE9BekNzQ3FEUkJYWUNPU2xyU1E1eGZZcXp4Znc4U0ZvWDNIU2c9;NTc4MjY4QDMxMzkyZTM0MmUzMEpHMTlBNThYVEwweitDSXF5Tkh3TVB6UTBIMmViWGt0eVkzdEtxYnpMM3c9;NTc4MjY5QDMxMzkyZTM0MmUzMFo4R2FldDJpMndZSCs3SWRQcUdFc016YUhFaUVtVUxmb2prc3RYa05Ed2c9");

//             Microsoft.Extensions.Configuration.IConfigurationSection section = Configuration.GetSection("Logging");
//             using (var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole()))
//             {
//                 loggerFactory.AddConsole();
//                 loggerFactory.AddDebug();
//             }
        }
    }
}
