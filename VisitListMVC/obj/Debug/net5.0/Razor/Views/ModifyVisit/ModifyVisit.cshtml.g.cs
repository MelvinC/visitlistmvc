#pragma checksum "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3b25cd6a62a816c1cd638acedb14edc0dc7a9ef9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ModifyVisit_ModifyVisit), @"mvc.1.0.view", @"/Views/ModifyVisit/ModifyVisit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\_ViewImports.cshtml"
using VisitListMVC;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\_ViewImports.cshtml"
using VisitListMVC.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3b25cd6a62a816c1cd638acedb14edc0dc7a9ef9", @"/Views/ModifyVisit/ModifyVisit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3c4128b602fa556bf0ef158f664d57e7c3cf7eae", @"/Views/_ViewImports.cshtml")]
    public class Views_ModifyVisit_ModifyVisit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<VisitListMVC.Models.Visits.Visit>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("modifyForm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Update", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "DeletePhoto", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("enctype", new global::Microsoft.AspNetCore.Html.HtmlString("multipart/form-data"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddPhoto", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
  
    ViewData["Title"] = "Modify Visit";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Modify your Visit</h1>\r\n\r\n<a>");
#nullable restore
#line 8 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
Write(Model.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n<a>");
#nullable restore
#line 9 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
Write(Model.StartDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n<a>");
#nullable restore
#line 10 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
Write(Model.EndDate);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3b25cd6a62a816c1cd638acedb14edc0dc7a9ef96228", async() => {
                WriteLiteral("\r\n    <table>\r\n        <tr>\r\n            <td>\r\n                <a>Name :</a>\r\n            </td>\r\n            <td>\r\n                <input name=\"Name\"");
                BeginWriteAttribute("value", " value=\"", 419, "\"", 438, 1);
#nullable restore
#line 19 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 427, Model.Name, 427, 11, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n                <a>Start date :</a>\r\n            </td>\r\n            <td>\r\n                <input name=\"StartDate\"");
                BeginWriteAttribute("value", " value=", 621, "", 656, 1);
#nullable restore
#line 27 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 628, Model.getStartDateDisplay(), 628, 28, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"date\">\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n                <a>End Date :</a>\r\n            </td>\r\n            <td>\r\n                <input name=\"EndDate\"");
                BeginWriteAttribute("value", " value=", 846, "", 879, 1);
#nullable restore
#line 35 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 853, Model.getEndDateDisplay(), 853, 26, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"date\">\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td>\r\n                <a>Comment :</a>\r\n            </td>\r\n            <td>\r\n                <input name=\"Comment\"");
                BeginWriteAttribute("value", " value=\"", 1068, "\"", 1090, 1);
#nullable restore
#line 43 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 1076, Model.Comment, 1076, 14, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"text\">\r\n            </td>\r\n        </tr>\r\n    </table>\r\n    <input name=\"Id\"");
                BeginWriteAttribute("value", " value=", 1174, "", 1190, 1);
#nullable restore
#line 47 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 1181, Model.Id, 1181, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"hidden\">\r\n    <button class=\"btn btn-danger\" form=\"modifyForm\">Modify</button>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 12 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
                                                          WriteLiteral(Model);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n<table>\r\n");
#nullable restore
#line 51 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
     foreach (var photo in Model.PhotoList)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                <a>Photo n°");
#nullable restore
#line 55 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
                       Write(Model.PhotoList.IndexOf(@photo)+1);

#line default
#line hidden
#nullable disable
            WriteLiteral(" :</a>\r\n            </td>\r\n            <td>\r\n                <img");
            BeginWriteAttribute("src", " src=\"", 1507, "\"", 1527, 2);
            WriteAttributeValue("", 1513, "/Photos/", 1513, 8, true);
#nullable restore
#line 58 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 1521, photo, 1521, 6, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 1528, "\"", 1575, 3);
            WriteAttributeValue("", 1534, "Photo", 1534, 5, true);
            WriteAttributeValue(" ", 1539, "n°", 1540, 3, true);
#nullable restore
#line 58 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue(" ", 1542, Model.PhotoList.IndexOf(@photo), 1543, 32, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" style=\"height: 300px;\">\r\n            </td>\r\n            <td>\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3b25cd6a62a816c1cd638acedb14edc0dc7a9ef913439", async() => {
                WriteLiteral("Delete");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-visitId", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 61 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
                                                  WriteLiteral(Model.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["visitId"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-visitId", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["visitId"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 61 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
                                                                            WriteLiteral(photo);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["photo"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-photo", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["photo"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 64 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</table>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3b25cd6a62a816c1cd638acedb14edc0dc7a9ef916608", async() => {
                WriteLiteral("\r\n    <input type=\"file\" name=\"UploadPhoto\" />\r\n    <input type=\"hidden\"");
                BeginWriteAttribute("value", " value=", 1969, "", 1985, 1);
#nullable restore
#line 68 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
WriteAttributeValue("", 1976, Model.Id, 1976, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" name=\"Id\" />\r\n    <input type=\"submit\" />\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-visitId", "Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 66 "D:\Solutec\.net\VisitListMVC\VisitListMVC\Views\ModifyVisit\ModifyVisit.cshtml"
                                                                              WriteLiteral(Model.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["visitId"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-visitId", __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["visitId"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<VisitListMVC.Models.Visits.Visit> Html { get; private set; }
    }
}
#pragma warning restore 1591
